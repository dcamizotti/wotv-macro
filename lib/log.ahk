notify(message) {
  TrayTip, WOTV Auto, %message%, 10, 16
  debug("[notify] " . message)
  sleep 3000
}

error(message) {
  TrayTip, WOTV Auto, %message%, 30, 3
  debug("[error] " . message)
  sleep 3000
}

debug(message) {
  OutputDebug, % "[" . A_Now . "] " . message
}