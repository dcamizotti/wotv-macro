#SingleInstance, Force
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

ANDROID_TITLE = BlueStacks
GAME_CMD = "C:\Program Files\BlueStacks\HD-RunApp.exe" -json "{\"app_icon_url\": \"\"`, \"app_name\": \"WOTV FFBE\"`, \"app_url\": \"\"`, \"app_pkg\": \"com.square_enix.android_amazon.WOTVffbeww\"}"
ANDROID_WIDTH := 900 ;fixed width of android emulator. If you change this, the image recognition files may not work properly
global IMAGE_SEARCH_ENABLE := true

global ANDROID_ID
global EMULATOR_LAUNCH_WAIT := 180000 ;time (in ms) to wait for emulator to finish starting up
global CHALLENGE_WAIT := 300000 ;time (in ms) to wait after starting an auto-battle on a challenge - i.e. 300000ms = 5 minutes
global BATTLE_WAIT := 120000 ; time (in ms) to wait after starting an auto-battle

global GAME_EXE := GAME_CMD
global GAME_TITLE := ANDROID_TITLE

activateEmulator() {
  if (!WinExist(GAME_TITLE))
  {
    ANDROID_ID := WinExist(GAME_TITLE)
    Run %ComSpec% /c "%GAME_EXE%
    notify("Launching Android emulator...")
    WinWait %GAME_TITLE%
    sleep 4000
    WinGet ANDROID_ID, ID, %GAME_TITLE%
    debug("Emulator window found: " . ANDROID_ID)
    resizeWindow()
    waitForImageAndClick("gameStart", 3000)
  }
  ; resizeWindow()
  if (ANDROID_ID != WinExist(GAME_TITLE))
    ANDROID_ID := WinExist(GAME_TITLE)

  WinActivate ahk_id %ANDROID_ID%
}

killEmulator() {
  process, close, BlueStacks.exe
}

resizeWindow() {
  WinMove, ahk_id %ANDROID_ID%,,0,0, 900, 592
}
