#includeAgain lib/imageSize.ahk
SetBatchLines -1 ;-- To start

findFirstImageAndClick(fileNames, clickDelay := 50, delay := 300000, cooldown := 500) {
  sleep delay
  for i, fileName in fileNames {
    debug("[findFirstImageAndClick] searching button " . fileName)
    sleep clickDelay
    if (clickOnImage(fileName, cooldown)) {
      return true
    }
    else {
      continue
    }
  }
  return false
}

waitForImageAndClick(fileName, clickDelay := 50, delay := 300000, cooldown := 500, sleepTime := 500){
  ; debug("[waitForImageAndClick] searching for img named " . fileName)
  img := waitForImage(fileName, delay, sleepTime)
  if (img != false) {
    ; debug("[waitForImageAndClick] Found! Clicking now!")
    sleep clickDelay
    return clickOnImageCoords(img, cooldown)
  }
  else {
    ; error("[waitForImageAndClick] failed to click; waitForImage failed failed for img " . fileName)
    return false
  }
}

clickOnImage(fileName, cooldown := 100, xMultiplier := 0.5, yMultiplier := 0.5) {
  img := findImage(fileName)
  if (img != false) {
    return clickOnImageCoords(img, cooldown, xMultiplier, yMultiplier)
  }
  else {
    error("[clickOnImage] failed to click; waitForImage failed failed for img " . fileName)
    return false
  }
}

clickOnImageCoords(img, cooldown := 100, xMultiplier := 0.5, yMultiplier := 0.5){
  if (img != false) {
    ; debug("[clickOnImageCoords] Found! Clicking now!")
    imgX := img.x + (img.width * xMultiplier) 
    imgY := img.y + (img.height * yMultiplier)
    MouseClick , left, imgX, imgY
    sleep cooldown
    return true
  }
  else {
    ; error("[clickOnImageCoords] failed to click; waitForImage failed failed")
    return false
  }
}

waitForImage(fileName, delay := 300000, sleepTime := 500, msgComplete := "Found image", msgTimeout := "Image find timed out, continuing...", IMAGE_SEARCH_ENABLE := true) {
  if (IMAGE_SEARCH_ENABLE) {
    _starttime := A_TickCount 
    loop {
      img := findImage(fileName, imgX, imgY, imgWidth, imgHeight)
      elapsedtime := A_TickCount - _starttime
      ; debug("[waitForImageAndClick] elapsedtime: " . elapsedtime)
      if (img != false) {
        ; debug("[waitForImageAndClick] " . msgComplete)
        return img
        break
      } else if (elapsedtime > delay) {
        ; error("[waitForImageAndClick] " . msgTimeout)
        return false
        break
      }
    }
    sleep sleepTime
  } else {
    sleep %delay%
  }
}

global prevFilename := ""
global prevResult := {}
findImage(fileName, imgX := 0, imgY := 0, imgWidth := 0, imgHeight := 0) {
  activateEmulator()
  if (fileName == prevFilename) {
    return prevResult
  }
  file := "lib/img/" . fileName . ".png"
  imgX := 0
  ImageSearch, imgX, imgY, 0, 0, A_ScreenWidth, A_ScreenHeight, *TransBlack *20 %file%
  if (imgX or imgY) {
    GetImageSize(file, imgWidth, imgHeight)
    prevFilename := fileName
    prevResult := { x: imgX, y: imgY, width: imgWidth, height: imgHeight}
    return prevResult
  }
  else {
    prevFilename := ""
    return false
  }

}
