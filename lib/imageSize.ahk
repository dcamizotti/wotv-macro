#NoEnv
#SingleInstance Force
ListLines Off
SetBatchLines -1 ;-- To start

;-- Initialize
MaxFiles :=2000
FileCount:=0
ArrayOfFiles:=Array()
ArrayOfFile.SetCapacity(MaxFiles)
global fileCache := Array()

GetImageSize(p_File,ByRef r_Width:=0,ByRef r_Height:=0)
{
  Static Dummy27649730

  ;-- Seek origin
  ,SEEK_SET:=0
  ;-- Seek from the beginning of the file.
  ,SEEK_CUR:=1
  ;-- Seek from the current position of the file pointer.
  ,SEEK_END:=2
  ;-- Seek from the end of the file.  The Distance should usually
  ;   be a negative value

  ;-- Initialize
  FileSizeFound:=False
  r_Width:=r_Height:=0

  ;[========]
  ;[  Open  ]
  ;[========]
  if not File:=FileOpen(p_File,"r","CP0")
  {
    ;;;;;        l_Message:=SystemMessage(A_LastError)
    outputdebug,
    (ltrim join`s
    Function: %A_ThisFunc% -
    Unexpected return code from FileOpen function.
    A_LastError=%A_LastError% - %l_Message%
    p_File: %p_File%
    )

    Return False
  }

  ;;;;;    outputdebug % "File Length: " . File.Length

  ;-- Bounce if the file is not at least 30 bytes
  if (File.Length<30)
  {
    outputdebug,
    (ltrim join`s
    Function: %A_ThisFunc% -
    The file is too small to contain an image.
    p_File: %p_File%
    )

    File.Close()
    Return False
  }

  ;-- Read the first 30 bytes
  VarSetCapacity(FileData,30,0)
  File.RawRead(FileData,30)

  ;-- Convert the first 30 bytes to a string (Encoding=ANSI)
  ;
  ;   Note 1: This step allows some of the first 30 bytes to be evaluated as
  ;   a string of characters instead of having to evaluate each byte as a
  ;   number.
  ;
  ;   Note 2: The StrGet command will automatically stop converting data when
  ;   a null character is found so if the first 30 bytes include dynamic data,
  ;   it is possible that not all 30 characters will make it to the FileString
  ;   variable.  Hint: It is safe to use the leading characters in the
  ;   string but if there is any non-static or null characters after the
  ;   initial characters, it's best to reconvert the characters after the gap
  ;   if needed.  Ex: NewString:=StrGet(&FileData+12,4,"CP0")
  ;
  FileString:=StrGet(&FileData,30,"CP0")
  ;;;;;    outputdebug FileString: %FileString%

  ;[==========]
  ;[  Bitmap  ]
  ;[==========]
  if (SubStr(FileString,1,2)="BM")
  {
    ;;;;;        outputdebug Bitmap file.
    DIBHeaderSize:=NumGet(FileData,14,"UInt")
    ;;;;;        outputdebug DIBHeaderSize: %DIBHeaderSize%

    if (DIBHeaderSize=12) ;-- BITMAPCOREHEADER (Windows 2.0+)
    {
      outputdebug p_File: %p_File%
      outputdebug BITMAPCOREHEADER (Windows 2.0+)
      r_Width :=NumGet(FileData,18,"UShort")
      r_Height:=NumGet(FileData,20,"UShort")
      outputdebug r_Width: %r_Width%, r_Height: %r_Height%
      FileSizeFound:=True
    }
    else ;-- everything else
    {
      r_Width :=NumGet(FileData,18,"Int")
      r_Height:=Abs(NumGet(FileData,22,"Int"))
      ;;;;;            outputdebug r_Width: %r_Width%, r_Height: %r_Height%
      FileSizeFound:=True
    }
  }

  ;[=======]
  ;[  GIF  ]
  ;[=======]
  if !FileSizeFound
    and (SubStr(FileString,1,3)="GIF"
  and SubStr(FileString,4,3)="87a" or SubStr(FileString,4,3)="89a")
  {
    r_Width :=NumGet(FileData,6,"UShort")
    r_Height:=NumGet(FileData,8,"UShort")
    ;;;;;       outputdebug r_Width: %r_Width%, r_Height: %r_Height%
    FileSizeFound:=True
  }

  ;[=======]
  ;[  PNG  ]
  ;[=======]
  if !FileSizeFound
    and (NumGet(FileData,0,"UChar")=0x89
  and SubStr(FileString,2,5)="PNG`r`n"
  and NumGet(FileData,6,"UChar")=0x1A
  and NumGet(FileData,7,"UChar")=0x0A ;-- LF
  and StrGet(&FileData+12,4,"CP0")="IHDR")
  {
    ;;;;;        outputdebug PNG!
    r_Width :=ByteSwap(NumGet(FileData,16,"UInt"))
    r_Height:=ByteSwap(NumGet(FileData,20,"UInt"))
    ;;;;;       outputdebug r_Width: %r_Width%, r_Height: %r_Height%
    FileSizeFound:=True
  }

  ;[========]
  ;[  JPEG  ]
  ;[========]
  if !FileSizeFound
    and (NumGet(FileData,0,"UChar")=0xFF
  and NumGet(FileData,1,"UChar")=0xD8
  and NumGet(FileData,2,"UChar")=0xFF)
  {
    ;;;;;        outputdebug JPEG file
    ;;;;;        IDString:=StrGet(&FileData+6,4,"CP0")
    ;;;;;        outputdebug IDString: %IDString%

    ;-- Loop through the tags
    FilePos:=2
    File.Seek(FilePos,SEEK_SET)
    ThisByte:=File.ReadUChar()

    ;;;;;            outputdebug % "Before the loop -  ThisByte: " . Format("0x{:X}",ThisByte)
    While (ThisByte=0xFF)
    {
      ;;;;;           outputdebug % "Top of the loop -  ThisByte: " . Format("0x{:X}",ThisByte)
      ThisByte:=File.ReadUChar()

            /*
                #####
                Note: Only found images with 0xC0 and 0xC2 tags.  Need to find
                all the formats to ensure that this works right.
      */
      ;;;;;           if (ThisByte>=0xC0 and ThisByte<=0xC3)
      if ThisByte Between 0xC0 and 0xC3
      {
        ;;;;;               outputdebug % "Desire header Hit! -  ThisByte: " . Format("0x{:X}",ThisByte)

        if (ThisByte<>0xC0 and ThisByte<>0xC2)
          outputdebug % "Found image with odd tag!: " . Format("0x{:X}",ThisByte)
        . ", File: " . p_File

        File.Seek(3,SEEK_CUR)
        r_Height:=ByteSwap(File.ReadUShort(),"UShort")
        r_Width :=ByteSwap(File.ReadUShort(),"UShort")
        ;;;;;                outputdebug r_Width: %r_Width%, r_Height: %r_Height%
        FileSizeFound:=True
        Break
      }

      ;;;;;            outputdebug % "Not the desired header.  Keep trying -  ThisByte: " . Format("0x{:X}",ThisByte)
      BlockSize:=ByteSwap(File.ReadUShort(),"UShort")
      ;;;;;           outputdebug BlockSize: %BlockSize%
      FilePos+=BlockSize+2
      File.Seek(FilePos,SEEK_SET)
      ThisByte:=File.ReadUChar()
      ;;;;;           outputdebug % "Bottom of the loop -  ThisByte: " . Format("0x{:X}",ThisByte)
    }
  }

  ;[===================]
  ;[        TIFF       ]
  ;[  (Little Endian)  ]
  ;[===================]
  if !FileSizeFound
    and (SubStr(FileString,1,2)="II"
  and NumGet(FileData,2,"UShort")=42) ;-- TIFF version
  {
    ;;;;;       outputdebug TIFF_II!

    ;-- Get the offset of the first Image File Directory (IFD)
    FilePos:=4
    File.Seek(FilePos,SEEK_SET)
    IFDOffset:=File.ReadUInt()
    ;;;;;       outputdebug IFDOffset: %IFDOffset%

    ;-- Collect the number of tags in the first IFD
    FilePos:=IFDOffset
    File.Seek(FilePos,SEEK_SET)
    IFDTagCount:=File.ReadUShort()
    ;;;;;       outputdebug IFDTagCount: %IFDTagCount%

    ;-- Update the file position
    FilePos:=File.Position

    ;-- Loop through the Image File Directory
    ;   Note: Only the Width and Height tags are used (for now)
    Loop %IFDTagCount%
    {
      ;;;;;           outputdebug % "Tag: " . A_Index
      TagID:=File.ReadUShort()
      ;;;;;            outputdebug % "TagID: " . TagID . ", Len: " . StrLen(TagID)
      ;;;;;            outputdebug % "TagID: " . Format("0x{:X}",TagID)

      ;-- Image Width
      if (TagID=0x100)
      {
        File.Seek(6,SEEK_CUR)
        r_Width:=File.ReadUInt()
        ;;;;;                outputdebug ########## r_Width: %r_Width%
      }

      ;-- Image Length (i.e. Height)
      if (TagID=0x101)
      {
        File.Seek(6,SEEK_CUR)
        r_Height:=File.ReadUInt()
        ;;;;;                outputdebug ########## r_Height: %r_Height%
      }

      ;-- Set the file position for the next tag
      ;   Note: Each tag is 12 bytes.  The file position is set on
      ;   every iteration so that regardless of what file activity is
      ;   performed in the loop, the file will be positioned correctly
      ;   for the next tag.
      FilePos+=12
      File.Seek(FilePos,SEEK_SET)
    }

        /*
            #####
            For now, assume that we got what we were looking for.  This might
            change in the future.
    */
    ;;;;;        outputdebug r_Width: %r_Width%, r_Height: %r_Height%
    FileSizeFound:=True
  }

  ;[================]
  ;[      TIFF      ]
  ;[  (Big Endian)  ]
  ;[================]
  if !FileSizeFound
    and (SubStr(FileString,1,2)="MM"
  and ByteSwap(NumGet(FileData,2,"UShort"),"UShort")=42) ;-- TIFF version
  {
    outputdebug TIFF_MM!

    ;-- Get the offset of the first Image File Directory (IFD)
    FilePos:=4
    File.Seek(FilePos,SEEK_SET)
    IFDOffset:=ByteSwap(File.ReadUInt())
    outputdebug IFDOffset: %IFDOffset%

    ;-- Collect the number of tags in the first IFD
    FilePos:=IFDOffset
    File.Seek(FilePos,SEEK_SET)
    IFDTagCount:=ByteSwap(File.ReadUShort(),"UShort")
    outputdebug IFDTagCount: %IFDTagCount%

    ;-- Update the file position
    FilePos:=File.Position

    ;-- Loop through the Image File Directory
    ;   Note: Only the Width and Height tags are used (for now)
    Loop %IFDTagCount%
    {
      outputdebug % "Tag: " . A_Index
      TagID:=ByteSwap(File.ReadUShort(),"UShort")
      outputdebug % "TagID: " . TagID . ", Len: " . StrLen(TagID)
      outputdebug % "TagID: " . Format("0x{:X}",TagID)

      ;-- Image Width
      if (TagID=0x100)
      {
        File.Seek(6,SEEK_CUR)
        r_Width:=ByteSwap(File.ReadUInt())
        outputdebug ########## r_Width: %r_Width%
      }

      ;-- Image Length (i.e. Height)
      if (TagID=0x101)
      {
        File.Seek(6,SEEK_CUR)
        r_Height:=ByteSwap(File.ReadUInt())
        outputdebug ########## r_Height: %r_Height%
      }

      ;-- Set the file position for the next tag
      ;   Note: Each tag is 12 bytes.  The file position is set on
      ;   every iteration so that regardless of what file activity is
      ;   performed in the loop, the file will be positioned correctly
      ;   for the next tag.
      FilePos+=12
      File.Seek(FilePos,SEEK_SET)
    }

        /*
            #####
            For now, assume that we found what we were looking for.  This might
            change in the future.
    */
    outputdebug r_Width: %r_Width%, r_Height: %r_Height%
    FileSizeFound:=True
  }

  ;[========]
  ;[  WebP  ]
  ;[========]
  if !FileSizeFound
    and (SubStr(FileString,1,4)="RIFF"
  and StrGet(&FileData+8,4,"CP0")="WEBP")
  {
    VP8Format:=StrGet(&FileData+12,4,"CP0")
    ;;;;;        outputdebug WEBP!
    if (VP8Format="VP8 ")
    {
      ;-- https://wiki.tcl-lang.org/page/Reading+WEBP+image+dimensions
      ;-- https://tools.ietf.org/html/rfc6386#page-30

      ;-- Check for valid code block identifier
      if (NumGet(FileData,23,"UChar")<>0x9D
        or NumGet(FileData,24,"UChar")<>0x01
      or NumGet(FileData,25,"UChar")<>0x2A)
      outputdebug Missing start code block!
      else
      {
        ;;;;;                outputdebug VP8
        File.Seek(26,SEEK_SET)

        ;-- Collect the width and height
        ;   Note: Each size field is stored in a 16-bit space but only
        ;   14-bits are used.  Since the program reads a UShort value
        ;   (16-bit unsigned integer), the high 2 bits are truncated to
        ;   ensure that correct value is returned.
        r_Width :=File.ReadUShort()&0x3FFF
        r_Height:=File.ReadUShort()&0x3FFF
        ;;;;;                outputdebug r_Width: %r_Width%, %r_Height%
        FileSizeFound:=True
      }
    }
    else if (VP8Format="VP8L")
    {
      ;-- https://wiki.tcl-lang.org/page/Reading+WEBP+image+dimensions
      ;;;;;            outputdebug VP8L
      File.Seek(16,SEEK_SET)
      Size :=File.ReadUInt() ;-- Not used (yet)
      Signature:=File.ReadUChar() ;-- Not used (yet)

      b0:=File.ReadUChar()
      b1:=File.ReadUChar()
      b2:=File.ReadUChar()
      b3:=File.ReadUChar()
      r_width :=1+(((b1&0x3F)<<8)|b0)
      r_Height:=1+(((b3&0xF)<<10)|(b2<<2)|((b1&0xC0)>>6))
      FileSizeFound:=True
    }
    else if (VP8Format="VP8X")
    {
      ;-- https://github.com/webmproject/webp-wic-codec/blob/master/src/libwebp/dec/webp.c
      ;;;;;            outputdebug VP8X
      File.Seek(24,SEEK_SET)
      b0:=File.ReadUChar()
      b1:=File.ReadUChar()
      b2:=File.ReadUChar()
      r_width:=1+(b2<<16|b1<<8|b0)

      b0:=File.ReadUChar()
      b1:=File.ReadUChar()
      b2:=File.ReadUChar()
      r_Height:=1+(b2<<16|b1<<8|b0)
      FileSizeFound:=True
    }
    else
      outputdebug % "WebP format not supported: " . VP8Format
  }

  ;[=============================]
  ;[             WMF             ]
  ;[  Aldus Placeable Metafiles  ]
  ;[          (D7CDC69A)         ]
  ;[=============================]
  if !FileSizeFound
    and (NumGet(FileData,0,"UInt")=0x9AC6CDD7)
  {
    ;;;;;        outputdebug WMF: D7CDC69A (AKA 9AC6CDD7)
    ;-- http://wvware.sourceforge.net/caolan/ora-wmf.html
    Left :=NumGet(FileData,6,"Short")
    Top :=NumGet(FileData,8,"Short")
    Right :=NumGet(FileData,10,"Short")
    Bottom:=NumGet(FileData,12,"Short")
    Inch :=NumGet(FileData,14,"UShort")
    ;-- The number of metafile units per inch

    r_Width :=Round((Right-Left)/Inch*A_ScreenDPI)
    r_Height:=Round((Bottom-Top)/Inch*A_ScreenDPI)
    FileSizeFound:=True
  }

  ;[=======]
  ;[  EMF  ]
  ;[=======]
  if !FileSizeFound
    and (NumGet(FileData,0,"UInt")=0x1) ;-- 4 bytes
  {
    ;-- Get the frame size (measured in 0.01 millimeter units)
    File.Seek(24,SEEK_SET)
    FrameLeft :=File.ReadInt()
    FrameTop :=File.ReadInt()
    FrameRight :=File.ReadInt()
    FrameBottom:=File.ReadInt()
    FrameWidth :=FrameRight-FrameLeft
    FrameHeight:=FrameBottom-FrameTop
    ;;;;;        outputdebug FrameLeft, Top, Right, Bottom, %FrameLeft%, %FrameTop%, %FrameRight%, %FrameBottom%
    ;;;;;        outputdebug FrameWidth, Height: %FrameWidth%, %FrameHeight%

    ;-- Get the reference device sizes (in pixels and in millimeters)
    File.Seek(72,SEEK_SET)
    WidthDevPixels :=File.ReadInt()
    HeightDevPixels:=File.ReadInt()
    WidthDevMM :=File.ReadInt()
    HeightDevMM :=File.ReadInt()

    ;-- Calculate size
    r_Width :=1+Round(WidthDevPixels*(FrameWidth/100/WidthDevMM))
    r_Height:=1+Round(HeightDevPixels*(FrameHeight/100/HeightDevMM))
    ;;;;;        outputdebug r_Width: %r_Width%, r_Height: %r_Height%
    FileSizeFound:=True
  }

  ;-- Shut it down
  File.Close()
  fileCache[p_File] := {Width:r_Width,Height:r_Height}
  Return FileSizeFound ? {Width:r_Width,Height:r_Height}:False
}

ByteSwap(p_Nbr,p_Type:="")
{
  if InStr(p_Type,"Short")
    Return ((p_Nbr&0xFF)<<8|(p_Nbr&0xFF00)>>8)
  else
    Return (p_Nbr&0xFF)<<24|(p_Nbr&0xFF00)<<8|(p_Nbr&0xFF0000)>>8|(p_Nbr&0xFF000000)>>24
}
