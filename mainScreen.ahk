#include lib/includes.ahk

global loginButtons := ["buttonNo", "mainDailyLogin", "buttonClose", "buttonCancel", "buttonDontDisplay", "buttonCloseX"]

accessGame() {
  notify("Accessing Game.")
  debug("[accessGame] ActivateEmulator")
  activateEmulator()
  debug("[accessGame] Clicking on the gameStart img")
  clickOnImage("gameStart")
}

goToMainScreen() {
  debug("[goToMainScreen] Checking if in mainscreen...")

  if (findImage("mainFarplane")) {
    debug("[goToMainScreen] already at main menu! Returning...")
    notify("At main screen.")
    return 
  }

  debug("[goToMainScreen] Trying to access menu button...")
  if (clickOnImage("buttonMainMenu")) {
    debug("[goToMainScreen] Success! Accessing home button now")
  }
  else {
    debug("[goToMainScreen] Checking for close buttons")
    findFirstImageAndClick(loginButtons, 1, 1)
    debug("[goToMainScreen] no more close button, so all should be good!")
  }

  goToMainScreen()
}
