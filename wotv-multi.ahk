﻿#NoEnv
#include lib/includes.ahk
#include mainScreen.ahk
SetBatchLines -1 ;-- To start

global timesRestarted := 0
global successfulGames := 0
global playersKicked := 0
global currentMethod := "none"
global doSolo := true
global doSoloWaitTime := 10000
global multiBanner := "multiBanner"
global multiQuest := "multiQuest"
global lastMethodChange := A_TickCount
global emergencyRestartLimit := 600000

Gui New, -MaximizeBox
Gui Add, Button, x40 y180 w50 h23, Start
Gui Add, Button, x100 y180 w50 h23, Stop
Gui Add, Button, x155 y180 w50 h23, Reset
Gui Add, Button, x210 y180 w50 h23, Resume
Gui Font, s14
Gui Add, Text, x20 y6 w350 h23 +0x200 Center, WOTV
Gui Font
Gui Add, Text, x64 y36 w350 h23 +0x200, timesRestarted, %timesRestarted%
Gui Add, Text, x64 y60 w350 h23 +0x200, successfulGames, %successfulGames%
Gui Add, Text, x64 y84 w350 h23 +0x200, playersKicked, %playersKicked%
Gui Add, Text, x64 y110 w350 h23 +0x200, currentMethod, %currentMethod%
Gui Show, x1560 y145 w350 h210, WOTV Multi Farmer -- Host
whereAmI()
Return

OnError("restartEverything")

addSuccessfulGames() {
  successfulGames += 1
  GuiControl,, successfulGames, successfulGames %successfulGames%
}
addPlayersKicked() {
  playersKicked += 1
  GuiControl,, playersKicked, playersKicked %playersKicked%
}
addTimesRestarted() {
  timesRestarted += 1
  GuiControl,, timesRestarted, timesRestarted %timesRestarted%
}
updateCurrentMethod(method) {
  if (currentMethod == method) {
    debug("[MULTI][updateCurrentMethod] still in the same method " . method . "; last change was at " . lastMethodChange)
    if (A_TickCount >= lastMethodChange + emergencyRestartLimit) {

      restartEverything()
    }

  }
  else {
    debug("[MULTI][updateCurrentMethod] updating to " . method)
    lastMethodChange := A_TickCount
    currentMethod := method
    GuiControl,, currentMethod, currentMethod %currentMethod%
  }
}

start() {
  updateCurrentMethod("start")
  addTimesRestarted()
  notify("Starting MULTI farmer!")
  debug("[MULTI] starting main screen")
  accessGame()
  goToMainScreen()
  accessMulti()
}

returnToTitle() {
  clickOnImage("buttonReturnToTitle")
  accessGame()
}

whereAmI() {
  updateCurrentMethod("whereAmI")
  debug("[MULTI] trying to figure out where I am")
  if (findImage("mainFarplane"))
    accessMulti()
  else if(findImage("buttonReturnToTitle"))
    returnToTitle()
  else if (findImage("multiKickPlayer")) 
    waitPlayerConfirm()
  else if(findImage("buttonDisbandRoom")) 
    waitForPlayers()
  else if (findImage("multiLabel"))
    createRoom()
  else if(findImage("ingameAutoOn"))
    waitForGameEnd()
  else if (findImage("buttonNext")) 
    confirmResults()
  else if (findImage("buttonMainMenu"))
    start()
  else if (findImage("gameStart"))
    start()
  else if ( waitForImage("popupMultiQuestResume", 10000))
    tryRejoinMulti()
  else
    restartEverything()
}

accessMulti() {
  updateCurrentMethod("accessMulti")
  debug("[MULTI] accessing multi")
  if(!waitForImageAndClick("mainFarplane"))
    restartEverything()
  waitForImageAndClick("farplaneMulti")
  createRoom()
}

createRoom() {
  updateCurrentMethod("createRoom")
  debug("[MULTI] creating multi room")
  i := 0
  waitForImage("multiLabel")
  while (!clickOnImage(multiBanner) and i < 10) {
    waitForImageAndClick("buttonScrollDown")
    i += 1
  }

  if(!waitForImageAndClick(multiQuest))
    restartEverything()

  else {
    waitForImageAndClick("multiCreateRoom")
    waitForImageAndClick("multiCreateRoomButton")
    waitForPlayers()
  }
}

waitForPlayers() {
  updateCurrentMethod("waitForPlayers")
  notify("Waiting for players to join")
  if(waitForImage("multiKickPlayer", doSoloWaitTime)) {
    waitPlayerConfirm()
  }
  else if(findImage("multiEmbarkButton") and doSolo) {
    notify("Going solo!")
    startGame()
  }
  else {
    whereAmI()
  }
}

waitPlayerConfirm() {
  updateCurrentMethod("waitPlayerConfirm")
  notify("We have players! Waiting for them to confirm. They have 10 seconds to confirm")

  ; check if there's still players in the room, if there are, kick them
  if (waitForImageAndClick("multiEmbarkButton", 50, doSoloWaitTime, 1, 50)) {
    startGame()
  }
  else if (findImage("multiKickPlayer")) {
    kickEveryone()
  }
  else {
    waitForPlayers()
  }
}

kickEveryone() {
  updateCurrentMethod("kickEveryone")
  notify("Kicking everyone out.")
  kickButtons := ["multiUnconfirmedPlayer", "multiUnconfirmedPlayer2"]

  if (clickOnImage("multiKickCheckingResults") or clickOnImage("multiUnconfirmedPlayer", 100, 0.5, 0.999)) {
    waitForImageAndClick("buttonKick")
    debug("[MULTI] kicked one")
    addPlayersKicked()
  }
  sleep 100
  if (!clickOnImage("multiEmbarkButton"))
    kickEveryone()
  else
    startGame()
}

startGame() {
  updateCurrentMethod("startGame")
  notify("Starting game!")
  debug("[startGame] clicking embark")
  clickOnImage("multiEmbarkButton", 0)
  debug("[startGame] clicking embark confirm")
  waitForImageAndClick("multiEmbarkConfirmButton", 150, 2000, 500, 50)
  debug("[startGame] clicked everything")
  waitForGameEnd()
}

waitForGameEnd() {
  updateCurrentMethod("waitForGameEnd")
  ; confirm everything is running dandy
  debug("[MULTI] game running -- waiting for it to end")
  if(waitForImage("buttonNext", 5000)){
    notify("Game finished!")
    ; confirmResults()
    Reload
  }
  else if (waitForImage("ingameAutoOn", 60000)){
    debug("[MULTI] still ingame")
    waitForGameEnd()
  }
  else if(waitForImage("multiConfirmInRoom", 2000)){
    debug("[MULTI] oops, looks like still in the waiting room")
    waitForPlayers()
  }
  else {
    debug("[MULTI] i'm lost!")
    whereAmI()
  }
}

confirmResults() {
  updateCurrentMethod("confirmResults")
  debug("[MULTI] confirmResults - denying friend requests")

  clickOnImage("buttonNext")
  waitForImageAndClick("buttonCancel", 750, 5000)
  debug("[MULTI] confirmResults - returning to room")
  if(!waitForImageAndClick("buttonReturn", 750, 10000))
    whereAmI()
  else {
    addSuccessfulGames()

    notify("Game ended! Heading back to the room.")
    if (waitForImage("buttonDisbandRoom"), 15000) {
      waitForPlayers()
    }
    else {
      whereAmI()
    }
  }
}

tryRejoinMulti(){
  updateCurrentMethod("tryRejoinMulti")
  debug("[MULTI] Trying to rejoin multi...")
  if(waitForImage("popupMultiQuestResume", 5000) ){
    debug("[MULTI] found the popup! lets go!")
    waitForImageAndClick("buttonYes", 500, 5000)
    if (waitForImageAndClick("buttonReturnToTitle", 500, 5000))
      return false
    return true
  }
  debug("[MULTI] nope, didnt work")
  return false
}

restartEverything() {
  notify("Something terrible happenned! Restarting everything!")
  updateCurrentMethod("restartEverything")
  killEmulator()
  addTimesRestarted()
  accessGame()
  if(tryRejoinMulti())
    waitForGameEnd()
  else
    start()
}

ButtonStart:
  start()

ButtonReset:
  Reload

ButtonStop:
Escape:
GuiClose:
ExitApp

ButtonResume:
  whereAmI()
